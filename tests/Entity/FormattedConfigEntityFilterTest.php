<?php

namespace PostInstallTest\Entity;

use PostInstall\Entity\FormattedConfigEntity;

/**
 * PostInstallTest\Entity\FormattedConfigEntityFilterTest
 * @package mihac\PostInstallTest\Entity
 */
class FormattedConfigEntityFilterTest extends \PHPUnit_Framework_TestCase
{
    /** @var FormattedConfigEntity */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new FormattedConfigEntity();
    }

    public function testIfCanMutateService()
    {
        $expected = 'foo';
        $this->fixture->setService($expected);
        $this->assertEquals($expected, $this->fixture->getService());
    }

    public function testIfCanMutatePath()
    {
        $expected = 'foo';
        $this->fixture->setPath($expected);
        $this->assertEquals($expected, $this->fixture->getPath());
    }

    public function testIfCanMutateUser()
    {
        $expected = 'foo';
        $this->fixture->setUser($expected);
        $this->assertEquals($expected, $this->fixture->getUser());
    }

    public function testIfCanMutateGroup()
    {
        $expected = 'foo';
        $this->fixture->setGroup($expected);
        $this->assertEquals($expected, $this->fixture->getGroup());
    }

    public function testIfCanMutateReplace()
    {
        $expected = 'foo';
        $this->fixture->setReplace($expected);
        $this->assertEquals($expected, $this->fixture->getReplace());
    }

    public function testIfCanMutateMode()
    {
        $expected = 'foo';
        $this->fixture->setMode($expected);
        $this->assertEquals($expected, $this->fixture->getMode());
    }

    public function testIfCanMutateTarget()
    {
        $expected = 'foo';
        $this->fixture->setTarget($expected);
        $this->assertEquals($expected, $this->fixture->getTarget());
    }

    public function testIfCanMutateBackup()
    {
        $expected = 'foo';
        $this->fixture->setBackup($expected);
        $this->assertEquals($expected, $this->fixture->getBackup());
    }
}
