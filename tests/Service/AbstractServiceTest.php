<?php

namespace PostInstallTest\Service;

use Mockery\MockInterface;
use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Entity\ObjectType;
use PostInstall\Service\Directory\DirectoryService;
use PostInstall\Service\File\FileService;
use PostInstall\Service\Link\LinkService;

/**
 * PostInstallTest\Service\AbstractServiceTest
 * @package mihac\PostInstallTest\Service
 */
class AbstractServiceTest extends \PHPUnit_Framework_TestCase
{
    use ServiceManagerAwareTrait;

    /** @var \PostInstall\Service\AbstractService */
    private $fixture;
    /** @var string */
    private $dir = '/tmp/test/phpunitDir';
    /** @var string $backupDir */
    private $backupDir = '/tmp/backup-test';
    /** @var DirectoryService $dirService */
    private $dirService;

    public function setUp()
    {
        $this->fixture = new AbstractServiceMock();
        mkdir($this->dir, 0777, true);
        shell_exec('chmod -R 0777 ' . $this->dir);
        shell_exec('chown -R root:root ' . $this->dir);

        $this->dirService = $this->getServiceManager()->get('directory');
    }

    public function tearDown()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dir);
        $this->dirService->removeObject($entity);

        $entity->setPath($this->backupDir);
        $this->dirService->removeObject($entity);
    }

    private function getMode($dir)
    {
        clearstatcache(true, $dir);
        $filePerms = fileperms($dir);
        return substr(sprintf('%o', $filePerms), -4);
    }

    private function getOwner($dir)
    {
        $owner = posix_getpwuid(fileowner($dir));
        $ownerName = $owner['name'];
        $group = posix_getgrgid(filegroup($dir));
        $groupName = $group['name'];

        return $ownerName . ":" . $groupName;
    }

    public function testSetupPermissionsAndOwner()
    {
        $startMode = $this->getMode($this->dir);
        $this->assertEquals('0777', $startMode);

        $startOwner = $this->getOwner($this->dir);
        $this->assertEquals('root:root', $startOwner);
    }

    public function testIfCanApplyMode()
    {
        $newMode = '0775';
        $entity = new FormattedConfigEntity();
        $entity->setMode($newMode);
        $entity->setPath($this->dir);

        $this->fixture->applyMode($entity);
        $this->assertEquals($newMode, $this->getMode($this->dir));
    }

    public function testIfCanApplyOwner()
    {
        $newOwner = "www-data";
        $oldGroup = "root";
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dir);
        $entity->setUser($newOwner);

        $this->fixture->applyOwner($entity);
        $this->assertEquals($newOwner . ":" . $oldGroup, $this->getOwner($this->dir));
    }

    public function testIfCanApplyGroup()
    {
        $newGroup = 'www-data';
        $oldOwner = "root";
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dir);
        $entity->setGroup($newGroup);

        $this->fixture->applyGroup($entity);
        $this->assertEquals($oldOwner . ":" . $newGroup, $this->getOwner($this->dir));
    }

    public function testIfCanCreateWithoutReplace()
    {
        $path = 'foo';
        $replace = false;
        $entity = new FormattedConfigEntity();
        $entity->setPath($path);
        $entity->setReplace($replace);

        /** @var AbstractServiceMock | MockInterface $mock */
        $mock = \Mockery::mock('PostInstallTest\Service\AbstractServiceMock[removeObject,createObject]');
        $mock->shouldReceive('createObject')->once()->with($entity);
        $mock->shouldReceive('removeObject')->never();

        $mock->createOrReplaceObject($entity);
    }

    public function testIfCanCreateAndReplace()
    {
        $path = 'foo';
        $replace = true;
        $entity = new FormattedConfigEntity();
        $entity->setPath($path);
        $entity->setReplace($replace);

        /** @var AbstractServiceMock | MockInterface $mock */
        $mock = \Mockery::mock('PostInstallTest\Service\AbstractServiceMock[removeObject,createObject,doesObjectExists]');
        $mock->shouldReceive('doesObjectExists')->with($entity)->andReturn(true);
        $mock->shouldReceive('createObject')->with($entity)->once();
        $mock->shouldReceive('removeObject')->with($entity)->once();

        $mock->createOrReplaceObject($entity);
    }

    public function testIfWillSkipRemovingObject()
    {
        $path = 'foo';
        $replace = true;
        $entity = new FormattedConfigEntity();
        $entity->setPath($path);
        $entity->setReplace($replace);

        /** @var AbstractServiceMock | MockInterface $mock */
        $mock = \Mockery::mock('PostInstallTest\Service\AbstractServiceMock[removeObject,createObject]');
        $mock->shouldReceive('doesObjectExists')->with($entity)->andReturn(false);
        $mock->shouldReceive('createObject')->with($entity)->once();
        $mock->shouldReceive('removeObject')->with($entity)->never();

        $mock->createOrReplaceObject($entity);
    }

    public function testIfCanCreateBackupDirectory()
    {
        $suffix = '_' . strtotime('now');
        $path = $this->backupDir . '/dir1';
        $expected = $path . $suffix;

        $entity = new FormattedConfigEntity();
        $entity->setPath($path);

        /** @var DirectoryService $service */
        $service = $this->getServiceManager()->get(ObjectType::DIRECTORY);
        $service->createObject($entity);
        $service->createBackup($entity, $suffix);
        $this->assertTrue(is_dir($expected));
    }

    public function testIfCanCreateBackupFile()
    {
        $suffix = '_' . strtotime('now');
        $path = $this->backupDir . '/file1';
        $expected = $path . $suffix;

        $entity = new FormattedConfigEntity();
        $entity->setPath($path);
        /** @var FileService $fileService */
        $fileService = $this->getServiceManager()->get('file');
        $fileService->createObject($entity);
        $fileService->createBackup($entity, $suffix);
        $this->assertTrue(is_file($expected));
    }

    public function testIfCanCreateBackupLink()
    {
        $suffix = '_' . strtotime('now');
        $path = $this->backupDir . '/link1';
        $expected = $path . $suffix;

        $entity = new FormattedConfigEntity();
        $entity->setPath($path);
        $entity->setTarget('/tmp/test');
        /** @var LinkService $linkService */
        $linkService = $this->getServiceManager()->get('link');
        $linkService->createObject($entity);
        $linkService->createBackup($entity, $suffix);
        $this->assertTrue(is_link($expected));
    }
}
