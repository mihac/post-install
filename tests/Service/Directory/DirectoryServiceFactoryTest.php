<?php

namespace PostInstallTest\Service\Directory;

use PostInstall\Service\Directory\DirectoryServiceFactory;

/**
 * PostInstallTest\Service\Directory\DirectoryServiceFactoryTest
 * @package mihac\PostInstallTest\Service\Directory
 */
class DirectoryServiceFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var DirectoryServiceFactory */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new DirectoryServiceFactory();
    }

    public function testInstance()
    {
        $this->assertInstanceOf('PostInstall\Base\Utils\FactoryInterface', $this->fixture);
    }

    public function testIfCanCreateService()
    {
        $service = $this->fixture->create();

        $this->assertInstanceOf('PostInstall\Service\Directory\DirectoryService', $service);
    }
}
