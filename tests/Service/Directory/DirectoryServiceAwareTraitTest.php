<?php

namespace PostInstallTest\Service\Directory;

/**
 * PostInstallTest\Service\Directory\DirectoryServiceAwareTraitTest
 * @package mihac\PostInstallTest\Service\Directory
 */
class DirectoryServiceAwareTraitTest extends \PHPUnit_Framework_TestCase
{
    /** @var DirectoryAwareTraitMock */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new DirectoryAwareTraitMock();
    }

    public function testIfCanThrowAnException()
    {
        $this->setExpectedException('RuntimeException');
        $this->fixture->getDirectoryService();
    }
}
