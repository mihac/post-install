<?php

namespace PostInstallTest\Service\Directory;

use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Service\Directory\DirectoryService;

/**
 * PostInstallTest\Service\DirectoryServiceTest
 * @package mihac\PostInstallTest\Service
 */
class DirectoryServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var DirectoryService */
    private $fixture;
    private $createDirPath = '/tmp/test/createDir';
    private $removeDirPath = '/tmp/test/removeDir';

    public function setUp()
    {
        $this->fixture = new DirectoryService();
    }

    public function tearDown()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->createDirPath);
        $this->fixture->removeObject($entity);
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->removeDirPath);
        $this->fixture->removeObject($entity);
    }

    public function testIfExtendsAbstractService()
    {
        $this->assertInstanceOf('PostInstall\Service\AbstractService', $this->fixture);
    }

    public function testIfCanCreateDirectory()
    {
        $dir = $this->createDirPath;
        $entity = new FormattedConfigEntity();
        $entity->setPath($dir);

        $this->assertFalse(is_dir($dir));

        $this->fixture->createObject($entity);

        $this->assertTrue(is_dir($dir));
    }

    public function testIfCanRemoveDirectory()
    {
        $dir = $this->removeDirPath;
        $entity = new FormattedConfigEntity();
        $entity->setPath($dir);

        $this->fixture->createObject($entity);

        $this->fixture->removeObject($entity);

        $this->assertFalse(is_dir($dir));
    }

    public function testIfCanDetermineIfObjectExists()
    {
        $dir = $this->createDirPath;
        $entity = new FormattedConfigEntity();
        $entity->setPath($dir);

        $this->assertFalse($this->fixture->doesObjectExists($entity));
        $this->fixture->createObject($entity);
        $this->assertTrue($this->fixture->doesObjectExists($entity));
    }

    public function testIfCanRecursivelyDelete()
    {
        $innerDir = "/tmp/recursive-test/step1/step3";
        $file = $innerDir . "/file.txt";
        $entity = new FormattedConfigEntity();
        $entity->setPath($innerDir);
        $this->fixture->createObject($entity);
        file_put_contents($file, "test");
        $this->assertTrue(is_file($file));

        $innerDir = "/tmp/recursive-test/step2/step4";
        $entity = new FormattedConfigEntity();
        $entity->setPath($innerDir);
        $this->fixture->createObject($entity);

        $removeDirPath = "/tmp/recursive-test";
        $entity->setPath($removeDirPath);
        $this->fixture->removeDirectoryRecursively($entity);
        $this->assertFalse(is_dir($removeDirPath));
    }
}
