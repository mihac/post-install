<?php

namespace PostInstallTest\Service\Directory;

use PostInstall\Service\Directory\DirectoryServiceAwareTrait;

/**
 * PostInstallTest\Service\Directory\DirectoryAwareTraitMock
 * @package mihac\PostInstallTest\Service\Directory
 */
class DirectoryAwareTraitMock
{
    use DirectoryServiceAwareTrait;
}
