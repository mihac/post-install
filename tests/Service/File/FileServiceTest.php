<?php

namespace PostInstallTest\Service\File;

use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Service\Directory\DirectoryService;
use PostInstall\Service\File\FileService;

/**
 * PostInstallTest\Service\File\FileServiceTest
 * @package mihac\PostInstallTest\Service\File
 */
class FileServiceTest extends \PHPUnit_Framework_TestCase
{
    use ServiceManagerAwareTrait;

    /** @var FileService */
    private $fixture;
    /** @var DirectoryService */
    private $directoryService;
    /** @var string */
    private $filePath;

    private $dirPath = '/tmp/fileCreation/';

    public function setUp()
    {
        $this->fixture = $this->getServiceManager()->get('file');
        $this->directoryService = $this->getServiceManager()->get('directory');
        $this->filePath = $this->dirPath . 'tmpFile.log';

        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dirPath);
        $this->directoryService->createObject($entity);
    }

    public function tearDown()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dirPath);
        $this->directoryService->removeObject($entity);
    }

    public function testIfIsInstanceOf()
    {
        $this->assertInstanceOf('PostInstall\Service\AbstractService', $this->fixture);
    }

    public function testIfCanDetermineIfFileExists()
    {
        if (file_exists($this->filePath)) {
            unlink($this->filePath);
        }
        $this->assertFalse(file_exists($this->filePath));

        $entity = new FormattedConfigEntity();
        $entity->setPath($this->filePath);
        $this->assertFalse($this->fixture->doesObjectExists($entity));

        touch($this->filePath);
        $this->assertTrue($this->fixture->doesObjectExists($entity));
    }

    public function testIfCanCreateObject()
    {
        $this->assertFalse(file_exists($this->filePath));

        $entity = new FormattedConfigEntity();
        $entity->setPath($this->filePath);
        $this->fixture->createObject($entity);

        $this->assertTrue(file_exists($this->filePath));
    }

    public function testIfCanRemoveFile()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->filePath);
        $this->fixture->createObject($entity);

        $this->fixture->removeObject($entity);
        $this->assertFalse(file_exists($this->filePath));
    }
}
