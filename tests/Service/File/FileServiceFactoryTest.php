<?php

namespace PostInstallTest\Service\File;

use PostInstall\Service\File\FileServiceFactory;

/**
 * PostInstallTest\Service\File\FileServiceFactoryTest
 * @package mihac\PostInstallTest\Service\File
 */
class FileServiceFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var FileServiceFactory */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new FileServiceFactory();
    }

    public function testTypeOfInstance()
    {
        $this->assertInstanceOf('PostInstall\Base\Utils\FactoryInterface', $this->fixture);

    }

    public function testIfCanCreateService()
    {
        $service = $this->fixture->create();
        $this->assertInstanceOf('PostInstall\Service\File\FileService', $service);
    }
}
