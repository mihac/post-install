<?php

namespace PostInstallTest\Service\Link;

use PostInstall\Service\Link\LinkServiceFactory;

/**
 * PostInstallTest\Service\Link\LinkServiceFactoryTest
 * @package mihac\PostInstallTest\Service\Link
 */
class LinkServiceFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var LinkServiceFactory */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new LinkServiceFactory();
    }

    public function testInstance()
    {
        $this->assertInstanceOf('PostInstall\Base\Utils\FactoryInterface', $this->fixture);
    }

    public function testIfCanCreateService()
    {
        $service = $this->fixture->create();
        $this->assertInstanceOf('PostInstall\Service\Link\LinkService', $service);
    }
}
