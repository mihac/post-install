<?php

namespace PostInstallTest\Service\Link;

use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Service\Directory\DirectoryService;
use PostInstall\Service\Link\LinkService;

/**
 * PostInstallTest\Service\Link\LinkServiceTest
 * @package mihac\PostInstallTest\Service\Link
 */
class LinkServiceTest extends \PHPUnit_Framework_TestCase
{
    use ServiceManagerAwareTrait;

    /** @var LinkService */
    private $fixture;
    /** @var DirectoryService */
    private $dirService;
    /** @var string */
    private $dirPath = '/tmp/testLink';
    /** @var string */
    private $linkPath;

    public function setUp()
    {
        $this->fixture = $this->getServiceManager()->get('link');
        $this->linkPath = $this->dirPath . '/testLink';
        $this->dirService = $this->getServiceManager()->get('directory');

        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dirPath);
        $entity->setReplace(1);
        $this->dirService->createOrReplaceObject($entity);
    }

    public function tearDown()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->dirPath);
        $this->dirService->removeObject($entity);
    }

    public function testInstance()
    {
        $this->assertInstanceOf('PostInstall\Service\AbstractService', $this->fixture);
    }

    public function testIfCanDetermineIfLinkExists()
    {
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->linkPath);
        $this->assertFalse($this->fixture->doesObjectExists($entity));

        symlink('/tmp/test', $entity->getPath());
        $this->assertTrue($this->fixture->doesObjectExists($entity));
    }

    public function testIfCanCreateLink()
    {
        $target = '/tmp/test';
        $entity = new FormattedConfigEntity();
        $entity->setPath($this->linkPath);
        $entity->setTarget($target);

        $this->assertFalse($this->fixture->doesObjectExists($entity));
        $this->fixture->createObject($entity);

        $this->assertTrue(is_link($entity->getPath()));
        $this->assertEquals($entity->getTarget(), readlink($entity->getPath()));
    }

    public function testIfCanRemoveObject()
    {
        $target = $this->dirPath . '/test-target';
        $entity = new FormattedConfigEntity();
        $entity->setPath($target);
        $this->dirService->createObject($entity);

        $entity->setPath($this->linkPath);
        $entity->setTarget($target);
        $this->fixture->createObject($entity);

        $this->fixture->removeObject($entity);
        $this->assertFalse(is_link($entity->getPath()));
        $this->assertTrue(is_dir($target));
    }
}
