<?php

namespace PostInstallTest\Service;

use Mockery\MockInterface;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Service\PostInstallService;

/**
 * PostInstallTest\Service\PostInstallServiceTest
 * @package mihac\PostInstallTest\Service
 */
class PostInstallServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var PostInstallService */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new PostInstallService();
    }

    public function testSomething()
    {
        $config = array();
        /** @var \PostInstall\Service\AbstractService | MockInterface $abstractServiceMock */
        $abstractServiceMock = \Mockery::mock('PostInstall\Service\AbstractService');
        $abstractServiceMock->shouldReceive('createOrReplaceObject')->once()->withAnyArgs();
        $abstractServiceMock->shouldReceive('applyOwner')->once()->withAnyArgs();
        $abstractServiceMock->shouldReceive('applyGroup')->once()->withAnyArgs();
        $abstractServiceMock->shouldReceive('applyMode')->once()->withAnyArgs();
        $abstractServiceMock->shouldReceive('createBackup')->once()->withAnyArgs();

        /** @var FormattedConfigEntity | MockInterface $formattedMock */
        $formattedMock = \Mockery::mock('PostInstall\Entity\FormattedConfigEntity');
        $formattedMock->shouldReceive('getService')->andReturn($abstractServiceMock);
        $formattedMock->shouldReceive('getPath')->andReturn('/tmp/test');
        $formattedMock->shouldReceive('getReplace')->andReturn(1);
        $formattedMock->shouldReceive('getUser')->andReturn('test');
        $formattedMock->shouldReceive('getGroup')->andReturn('testgroup');
        $formattedMock->shouldReceive('getMode')->andReturn('mode');
        $formattedMock->shouldReceive('getBackup')->andReturn('backup');

        $formatted = array($formattedMock);
        $filterMock = \Mockery::mock('PostInstall\Filter\ParseConfigFileFilter');
        $filterMock->shouldReceive('filter')->with($config)->andReturn($formatted);

        $serviceMock = \Mockery::mock('PostInstall\Service\PostInstallService[getParseConfigFileFilter]');
        $serviceMock->shouldReceive('getParseConfigFileFilter')->andReturn($filterMock);
        $serviceMock->init($config);
    }

    public function testIfActionsShouldNotPerform()
    {
        $config = array();
        /** @var \PostInstall\Service\AbstractService | MockInterface $abstractServiceMock */
        $abstractServiceMock = \Mockery::mock('PostInstall\Service\AbstractService');
        $abstractServiceMock->shouldReceive('createOrReplaceObject')->once()->withAnyArgs();
        $abstractServiceMock->shouldReceive('applyOwner')->never();
        $abstractServiceMock->shouldReceive('applyGroup')->never();
        $abstractServiceMock->shouldReceive('applyMode')->never();
        $abstractServiceMock->shouldReceive('createBackup')->never();

        /** @var FormattedConfigEntity | MockInterface $formattedMock */
        $formattedMock = \Mockery::mock('PostInstall\Entity\FormattedConfigEntity');
        $formattedMock->shouldReceive('getService')->andReturn($abstractServiceMock);
        $formattedMock->shouldReceive('getPath')->andReturn('/tmp/test');
        $formattedMock->shouldReceive('getReplace')->andReturn(false);
        $formattedMock->shouldReceive('getUser')->andReturn(false);
        $formattedMock->shouldReceive('getGroup')->andReturn(false);
        $formattedMock->shouldReceive('getMode')->andReturn(false);
        $formattedMock->shouldReceive('getBackup')->andReturn(false);

        $formatted = array($formattedMock);
        $filterMock = \Mockery::mock('PostInstall\Filter\ParseConfigFileFilter');
        $filterMock->shouldReceive('filter')->with($config)->andReturn($formatted);

        $serviceMock = \Mockery::mock('PostInstall\Service\PostInstallService[getParseConfigFileFilter]');
        $serviceMock->shouldReceive('getParseConfigFileFilter')->andReturn($filterMock);
        $serviceMock->init($config);
    }
}
