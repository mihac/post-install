<?php

namespace PostInstallTest\Filter;

use Mockery\MockInterface;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Filter\ParseConfigFileFilter;

/**
 * PostInstallTest\Filter\ParseConfigFileFilterTest
 * @package mihac\PostInstallTest\Filter
 */
class ParseConfigFileFilterTest extends \PHPUnit_Framework_TestCase
{
    public function testIfCanFilter()
    {
        $config = array(
            array(

            ),
        );
        $entity = new FormattedConfigEntity();
        $formattedConfigEntityFilter = \Mockery::mock('PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter');
        $formattedConfigEntityFilter
            ->shouldReceive('mapArrayToObject')
            ->once()
            ->with(array(), \Mockery::type('PostInstall\Entity\FormattedConfigEntity'))
            ->andReturn($entity);

        /** @var ParseConfigFileFilter | MockInterface $fixture */
        $fixture = \Mockery::mock('PostInstall\Filter\ParseConfigFileFilter[getFormattedConfigEntityFilter]');
        $fixture->shouldReceive('getFormattedConfigEntityFilter')
            ->once()
            ->andReturn($formattedConfigEntityFilter);

        $expected = array($entity);
        $this->assertEquals($expected, $fixture->filter($config));
    }
}
