<?php

namespace PostInstallTest\Filter;
use PostInstall\Filter\ParseConfigFileFilter;

/**
 * PostInstallTest\Filter\ParseConfigFileFilterAwareTraitTest
 * @package mihac\PostInstallTest\Filter
 */
class ParseConfigFileFilterAwareTraitTest extends \PHPUnit_Framework_TestCase
{
    /** @var FilterAwareClassMock */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new FilterAwareClassMock();
    }

    public function testIfCanGetFilter()
    {
        $this->assertInstanceOf('PostInstall\Filter\ParseConfigFileFilter', $this->fixture->getParseConfigFileFilter());
    }

    public function testIfCanSetFilter()
    {
        $expected = new ParseConfigFileFilter();
        $this->fixture->setParseConfigFileFilter($expected);

        $this->assertSame($expected, $this->fixture->getParseConfigFileFilter());
    }

    public function testIfCanThrowAnException()
    {
        $this->setExpectedException('DomainException');
        $this->fixture->setParseConfigFileFilter(new \stdClass());
    }
}
