<?php

namespace PostInstallTest\Filter\EntityFilter;

use PostInstall\Filter\EntityFilter\FormattedConfigEntityFilterAwareTrait;

/**
 * PostInstallTest\Filter\EntityFilter\FormattedConfigEntityFilterTraitMock
 * @package mihac\PostInstallTest\Filter\EntityFilter
 */
class FormattedConfigEntityFilterTraitMock
{
    use FormattedConfigEntityFilterAwareTrait;
}
