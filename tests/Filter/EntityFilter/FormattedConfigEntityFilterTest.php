<?php

namespace PostInstallTest\Filter\EntityFilter;

use Mockery\MockInterface;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter;

/**
 * PostInstallTest\Filter\EntityFilter\FormattedConfigEntityFilterTest
 * @package mihac\PostInstallTest\Filter\EntityFilter
 */
class FormattedConfigEntityFilterTest extends \PHPUnit_Framework_TestCase
{
    /** @var FormattedConfigEntityFilter */
    protected $fixture;
    /** @var FormattedConfigEntityFilter | MockInterface */
    protected $fixtureMock;

    public function setUp()
    {
        $this->fixture = new FormattedConfigEntityFilter();
        $this->fixtureMock = \Mockery::mock('PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter[getMandatoryConfigKeys,getServiceManager]');
        $this->fixtureMock->shouldReceive('getMandatoryConfigKeys')->andReturn(array('foo', 'bar', 'type'));
        $serviceManagerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager');
        $serviceManagerMock->shouldReceive('get')->with('stdClass')
            ->andReturn(new \stdClass());
        $this->fixtureMock->shouldReceive('getServiceManager')->andReturn($serviceManagerMock);
    }

    public function testIfCanGetMandatoryFields()
    {
        $config = include(__DIR__ . "/../../../config/config.php");
        $mandatoryFields = $config['mandatory_fields'];
        $this->assertEquals($mandatoryFields, $this->fixture->getMandatoryConfigKeys('directory'));

        $mandatoryFields = $config['mandatory_fields_link'];
        $this->assertEquals($mandatoryFields, $this->fixture->getMandatoryConfigKeys('link'));
    }

    public function testIfCanFilterThrowsAnException()
    {
        $config = array(
            'foo' => 'baz',
            'type' => 'bar',
        );

        $this->setExpectedException('DomainException');
        $this->fixtureMock->mapArrayToObject($config, new FormattedConfigEntity());
    }

    public function testIfCanFilterThrowsAnExceptionWhenTypeIsNotSet()
    {
        $config = array(
            'foo' => 'baz',
        );

        $this->setExpectedException('DomainException');
        $this->fixtureMock->mapArrayToObject($config, new FormattedConfigEntity());

    }

    public function testIfCanSetMandatoryConfigKeys()
    {
        $config = array('foo' => 5, 'baz' => 6, 'type' => 'bar');
        $this->assertFalse($this->fixtureMock->checkIfConfigurationHasAllRequiredKeys($config));

        $config = array('foo' => 5, 'bar' => 6, 'type' => 'directory');
        $this->assertTrue($this->fixtureMock->checkIfConfigurationHasAllRequiredKeys($config));

        $config = array('bar' => 5, 'foo' => 6, 'type' => 'directory');
        $this->assertTrue($this->fixtureMock->checkIfConfigurationHasAllRequiredKeys($config));

        $config = array('foo' => 5, 'type' => 'bar');
        $this->assertFalse($this->fixtureMock->checkIfConfigurationHasAllRequiredKeys($config));
    }

    public function testIfCanDetermineReplacement()
    {
        $config = array('replace' => 1);
        $this->assertEquals(1, $this->fixture->determineReplacement($config));

        $config = array('replace' => 0);
        $this->assertEquals(0, $this->fixture->determineReplacement($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineReplacement($config));
    }

    public function testIfCanDetermineUser()
    {
        $config = array('user' => 'foo');
        $this->assertEquals('foo', $this->fixture->determineUser($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineUser($config));
    }

    public function testIfCanDetermineGroup()
    {
        $config = array('group' => 'foo');
        $this->assertEquals('foo', $this->fixture->determineGroup($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineGroup($config));
    }

    public function testIfCanDetermineMode()
    {
        $config = array('mode' => 'foo');
        $this->assertEquals('foo', $this->fixture->determineMode($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineMode($config));
    }

    public function testIfCanDetermineTarget()
    {
        $config = array('target' => 'foo');
        $this->assertEquals('foo', $this->fixture->determineTarget($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineTarget($config));
    }


    public function testIfCanDetermineBackup()
    {
        $config = array('backup' => 'foo');
        $this->assertEquals('foo', $this->fixture->determineBackup($config));

        $config = array();
        $this->assertEquals(0, $this->fixture->determineBackup($config));
    }

    public function testIfCanBindArrayToObject()
    {
        $array = array(
            'type' => 'stdClass',
            'path' => 'bar',
        );
        /** @var FormattedConfigEntityFilter | MockInterface $fixtureMock */
        $fixtureMock = \Mockery::mock('PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter[getMandatoryConfigKeys,getServiceManager]');
        $fixtureMock->shouldReceive('getMandatoryConfigKeys')->andReturn(array('type', 'path'));
        $serviceManagerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager');
        $serviceManagerMock->shouldReceive('get')->with('stdClass')
            ->andReturn(new \stdClass());
        $fixtureMock->shouldReceive('getServiceManager')->andReturn($serviceManagerMock);
        $fixtureMock->shouldReceive('determineUser')->andReturn('foo1');
        $fixtureMock->shouldReceive('determineGroup')->andReturn('foo2');
        $fixtureMock->shouldReceive('determineMode')->andReturn('foo3');
        $fixtureMock->shouldReceive('determineReplacement')->andReturn('foo4');
        $fixtureMock->shouldReceive('determineTarget')->andReturn('foo5');
        $fixtureMock->shouldReceive('determineBackup')->andReturn('foo6');

        /** @var FormattedConfigEntity | \MockeryTest_Interface $entity */
        $entity = \Mockery::mock('PostInstall\Entity\FormattedConfigEntity');
        $entity->shouldReceive('setService')
            ->once()
            ->with(\Mockery::type('stdClass'));
        $entity->shouldReceive('setPath')
            ->once()
            ->with('bar');
        $entity->shouldReceive('setUser')
            ->once()
            ->with('foo1');
        $entity->shouldReceive('setGroup')
            ->once()
            ->with('foo2');
        $entity->shouldReceive('setMode')
            ->once()
            ->with('foo3');
        $entity->shouldReceive('setReplace')
            ->once()
            ->with('foo4');
        $entity->shouldReceive('setTarget')
            ->once()
            ->with('foo5');
        $entity->shouldReceive('setBackup')
            ->once()
            ->with('foo6');

        $fixtureMock->mapArrayToObject($array, $entity);
    }
}
