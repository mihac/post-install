<?php

namespace PostInstallTest\Filter\EntityFilter;

use PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter;

/**
 * PostInstallTest\Filter\EntityFilter\FormattedConfigEntityFilterAwareTraitTest
 * @package mihac\PostInstallTest\Filter\EntityFilter
 */
class FormattedConfigEntityFilterAwareTraitTest extends \PHPUnit_Framework_TestCase
{
    /** @var FormattedConfigEntityFilterTraitMock */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new FormattedConfigEntityFilterTraitMock();
    }

    public function testIfCanMutateEntityFilter()
    {
        $expected = new FormattedConfigEntityFilter();
        $this->fixture->setFormattedConfigEntityFilter($expected);

        $this->assertSame($expected, $this->fixture->getFormattedConfigEntityFilter());
    }

    public function testIfCanCreateAsSingletonEntityFilter()
    {
        $result = $this->fixture->getFormattedConfigEntityFilter();

        $this->assertInstanceOf('PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter', $result);
    }
}
