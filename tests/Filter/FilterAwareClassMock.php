<?php

namespace PostInstallTest\Filter;

use PostInstall\Filter\ParseConfigFileFilterAwareTrait;

/**
 * PostInstallTest\Filter\FilterAwareClassMock
 * @package mihac\PostInstallTest\Filter
 */
class FilterAwareClassMock
{
    use ParseConfigFileFilterAwareTrait;
}
