<?php

namespace PostInstallTest\Base\Service;

use PostInstall\Base\Utils\FactoryInterface;

/**
 * PostInstallTest\Base\Service\FactoryMock
 * @package mihac\PostInstallTest\Service
 */
class FactoryMock implements FactoryInterface
{
    /**
     * @return \stdClass
     */
    public function create()
    {
        return new \stdClass();
    }
}
