<?php

namespace PostInstallTest\Service;

use Mockery\MockInterface;
use PostInstall\Base\Service\ServiceManager;

/**
 * PostInstallTest\Service\ServiceManagerTest
 * @package mihac\PostInstallTest\Service
 */
class ServiceManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var ServiceManager */
    private $fixture;

    private $configMock = array();

    public function setUp()
    {
        $this->fixture = new ServiceManager();
        $this->configMock = array(
            'service_manager' => array(
                'invokables' => array(),
                'factories' => array(),
            )
        );
    }

    public function testIfCanDetermineConfig()
    {
        $expected = include(__DIR__ . '/../../../config/config.php');

        $this->assertEquals($expected, $this->fixture->getConfig());
    }

    public function testIfCanThrowsAnExceptionOnServiceManagerMissConfigured()
    {
        /** @var ServiceManager | MockInterface $managerMock */
        $managerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager[getConfig]');
        $managerMock
            ->shouldReceive('getConfig')
            ->andReturn(array());

        $this->setExpectedException('\DomainException');
        $managerMock->get('foo');
    }

    public function testIfCanInstantiateInvokable()
    {
        $config = array(
            'service_manager' => array(
                'invokables' => array(
                    'foo' => 'stdClass',
                ),
            ),
        );

        /** @var ServiceManager | MockInterface $managerMock */
        $managerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager[getConfig]');
        $managerMock
            ->shouldReceive('getConfig')
            ->andReturn($config);

        $this->assertEquals(new \stdClass(), $managerMock->get('foo'));
    }

    public function testIfCanRecognizeFactoryInterface()
    {
        $config = array(
            'service_manager' => array(
                'factories' => array(
                    'foo' => 'stdClass',
                ),
            ),
        );

        /** @var ServiceManager | MockInterface $managerMock */
        $managerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager[getConfig]');
        $managerMock
            ->shouldReceive('getConfig')
            ->andReturn($config);

        $this->setExpectedException('\DomainException');
        $managerMock->get('foo');
    }

    public function testIfCanInstantiateFactory()
    {
        $config = array(
            'service_manager' => array(
                'factories' => array(
                    'foo' => 'PostInstallTest\Base\Service\FactoryMock',
                ),
            ),
        );

        /** @var ServiceManager | MockInterface $managerMock */
        $managerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager[getConfig]');
        $managerMock
            ->shouldReceive('getConfig')
            ->andReturn($config);

        $this->assertEquals(new \stdClass(), $managerMock->get('foo'));
    }

    public function testIfNothingIsFound()
    {
        $config = array(
            'service_manager' => array(
            ),
        );

        /** @var ServiceManager | MockInterface $managerMock */
        $managerMock = \Mockery::mock('PostInstall\Base\Service\ServiceManager[getConfig]');
        $managerMock
            ->shouldReceive('getConfig')
            ->andReturn($config);

        $this->setExpectedException('\DomainException');
        $managerMock->get('foo');
    }
}
