<?php

namespace PostInstallTest\Base\Service;

use PostInstall\Base\Service\ServiceManagerAwareTrait;

/**
 * PostInstallTest\Base\Service\ServiceManagerAwareMock
 * @package mihac\PostInstallTest\Base\Service
 */
class ServiceManagerAwareMock
{
    use ServiceManagerAwareTrait;
}
