<?php

namespace PostInstallTest\Base\Service;
use PostInstall\Base\Service\ServiceManager;

/**
 * PostInstallTest\Base\Service\ServiceManagerAwareTraitTest
 * @package mihac\PostInstallTest\Base\Service
 */
class ServiceManagerAwareTraitTest extends \PHPUnit_Framework_TestCase
{
    /** @var ServiceManagerAwareMock */
    private $fixture;

    public function setUp()
    {
        $this->fixture = new ServiceManagerAwareMock();
    }

    public function testIfCanMutateServiceManager()
    {
        $result = $this->fixture->getServiceManager();
        $this->assertInstanceOf('PostInstall\Base\Service\ServiceManager', $result);

        $new = new ServiceManager();
        $this->fixture->setServiceManager($new);
        $this->assertSame($new, $this->fixture->getServiceManager());
    }

    public function testIfCanThrowAnException()
    {
        $this->setExpectedException('DomainException');
        $this->fixture->setServiceManager(true);
    }
}
