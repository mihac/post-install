<?php

namespace PostInstallTest\Base\Entity;

/**
 * Base\Entity\ConfigAwareTraitTest
 * @package mihac\Base\Entity
 */
class ConfigAwareTraitTest extends \PHPUnit_Framework_TestCase
{
    /** @var ConfigAwareClassMock */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new ConfigAwareClassMock();
    }

    public function testIfCanMutateConfigAttribute()
    {
        $this->assertEquals(array(), $this->fixture->getConfig());

        $config = array(0, 1, 2);
        $this->fixture->setConfig($config);

        $this->assertEquals($config, $this->fixture->getConfig());
    }

    public function testIfThrowsExceptionWhenConfigIsNotAnArray()
    {
        $this->setExpectedException('DomainException', 'Configuration is not an array');
        $this->fixture->setConfig('foo');
    }
}
