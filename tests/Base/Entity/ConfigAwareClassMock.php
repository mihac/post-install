<?php

namespace PostInstallTest\Base\Entity;

use PostInstall\Base\Entity\ConfigAwareTrait;

/**
 * PostInstallTest\Entity\ConfigAwareClassMock
 * @package mihac\PostInstallTest\Entity
 */
class ConfigAwareClassMock
{
    use ConfigAwareTrait;
}
