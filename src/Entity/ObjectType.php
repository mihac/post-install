<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\ObjectType
 * @package php-marketingbackend\PostInstall\Entity
 */
class ObjectType
{
    const DIRECTORY = 'directory';

    const FILE = 'file';

    const LINK = 'link';
}
