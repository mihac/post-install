<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\BackupAwareInterface
 * @package mihac\PostInstall\Entity
 */
interface BackupAwareInterface
{
    public function getBackup();

    public function setBackup($backup);
}
