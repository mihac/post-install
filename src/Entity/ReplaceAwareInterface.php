<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\ReplaceAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface ReplaceAwareInterface
{
    public function getReplace();
    public function setReplace($replace);
}
