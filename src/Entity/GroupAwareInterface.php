<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\GroupAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface GroupAwareInterface
{
    public function getGroup();
    public function setGroup($group);
}
