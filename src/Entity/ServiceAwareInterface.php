<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\ServiceAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface ServiceAwareInterface
{
    public function getService();
    public function setService($service);
}
