<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\PathAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface PathAwareInterface
{
    public function getPath();
    public function setPath($path);
}
