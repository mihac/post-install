<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\UserAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface UserAwareInterface
{
    public function getUser();
    public function setUser($user);
}
