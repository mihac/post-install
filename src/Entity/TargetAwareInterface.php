<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\TargetAwareInterface
 * @package mihac\PostInstall\Entity
 */
interface TargetAwareInterface
{
    public function getTarget();

    public function setTarget($target);
}
