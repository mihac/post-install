<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\ModeAwareInterface
 * @package php-marketingbackend\PostInstall\Entity
 */
interface ModeAwareInterface
{
    public function getMode();
    public function setMode($mode);
}
