<?php

namespace PostInstall\Entity;

/**
 * PostInstall\Entity\FormattedConfigEntity
 * @package mihac\PostInstall\Entity
 */
class FormattedConfigEntity implements
    GroupAwareInterface,
    UserAwareInterface,
    ModeAwareInterface,
    PathAwareInterface,
    ReplaceAwareInterface,
    TargetAwareInterface
{
    /** @var \PostInstall\Service\AbstractService */
    protected $service;
    /** @var string */
    protected $path;
    /** @var int */
    protected $replace;
    /** @var string | int */
    protected $user;
    /** @var string | int */
    protected $group;
    /** @var string | int */
    protected $mode;
    /** @var string | int */
    protected $target;
    /** @var string | int */
    protected $backup;

    /**
     * @return \PostInstall\Service\AbstractService
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param \PostInstall\Service\AbstractService $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getReplace()
    {
        return $this->replace;
    }

    /**
     * @param int $replace
     */
    public function setReplace($replace)
    {
        $this->replace = $replace;
    }

    /**
     * @return int|string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int|string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int|string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param int|string $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return int|string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param int|string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return int|string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param int|string $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return int|string
     */
    public function getBackup()
    {
        return $this->backup;
    }

    /**
     * @param int|string $backup
     */
    public function setBackup($backup)
    {
        $this->backup = $backup;
    }
}
