<?php

namespace PostInstall\Service;

use PostInstall\Entity\GroupAwareInterface;
use PostInstall\Entity\ModeAwareInterface;
use PostInstall\Entity\PathAwareInterface;
use PostInstall\Entity\ReplaceAwareInterface;
use PostInstall\Entity\UserAwareInterface;

/**
 * PostInstall\Service\AbstractService
 * @package mihac\PostInstall\Service
 */
abstract class AbstractService
{
    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    abstract public function createObject($entity);

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    abstract public function removeObject($entity);

    /**
     * @param PathAwareInterface $entity
     *
     * @return bool
     */
    abstract public function doesObjectExists($entity);

    /**
     * @param ModeAwareInterface | PathAwareInterface $entity
     *
     * @return bool
     */
    public function applyMode($entity)
    {
        $mode = $entity->getMode();
        $mode = intval($mode, 8);
        return chmod($entity->getPath(), $mode);
    }

    /**
     * @param UserAwareInterface | PathAwareInterface $entity
     *
     * @return bool
     */
    public function applyOwner($entity)
    {
        $owner = $entity->getUser();
        return chown($entity->getPath(), $owner);
    }

    /**
     * @param GroupAwareInterface | PathAwareInterface $entity
     *
     * @return bool
     */
    public function applyGroup($entity)
    {
        $group = $entity->getGroup();
        return chgrp($entity->getPath(), $group);
    }

    /**
     * @param PathAwareInterface | ReplaceAwareInterface $entity
     *
     * @return void
     */
    public function createOrReplaceObject($entity)
    {
        if ($entity->getReplace() && $this->doesObjectExists($entity)) {
            $this->removeObject($entity);
        }
        $this->createObject($entity);
    }

    /**
     * @param PathAwareInterface $entity
     * @param string $suffix
     *
     * @return void
     */
    public function createBackup($entity, $suffix)
    {
        $path = $entity->getPath();
        if ($this->doesObjectExists($entity)) {
            rename($path, $path . $suffix);
        }
    }
}
