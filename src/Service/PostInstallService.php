<?php

namespace PostInstall\Service;

use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Filter\ParseConfigFileFilterAwareTrait;

/**
 * PostInstall\Service\PostInstallService
 * @package mihac\PostInstall\Service
 */
class PostInstallService
{
    use ParseConfigFileFilterAwareTrait;

    /**
     * @param array $config
     */
    public function init($config)
    {
        $filteredConfigs = $this->getParseConfigFileFilter()->filter($config);
        /** @var FormattedConfigEntity $filteredConfig */
        foreach ($filteredConfigs as $filteredConfig) {
            if ($filteredConfig->getBackup()) {
                $suffix = '_' . date('Y-m-d_h:i:s');
                $filteredConfig->getService()->createBackup($filteredConfig, $suffix);
            }
            $filteredConfig->getService()->createOrReplaceObject($filteredConfig);
            if ($filteredConfig->getUser()) {
                $filteredConfig->getService()->applyOwner($filteredConfig);
            }
            if ($filteredConfig->getGroup()) {
                $filteredConfig->getService()->applyGroup($filteredConfig);
            }
            if ($filteredConfig->getMode()) {
                $filteredConfig->getService()->applyMode($filteredConfig);
            }
        }
    }
}
