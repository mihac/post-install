<?php

namespace PostInstall\Service\Link;

use PostInstall\Entity\PathAwareInterface;
use PostInstall\Entity\TargetAwareInterface;
use PostInstall\Service\AbstractService;
use PostInstall\Service\Directory\DirectoryServiceAwareTrait;

/**
 * PostInstall\Service\Link\LinkService
 * @package mihac\PostInstall\Service\Link
 */
class LinkService extends AbstractService
{
    use DirectoryServiceAwareTrait;

    /**
     * @param PathAwareInterface | TargetAwareInterface $entity
     *
     * @return void
     */
    public function createObject($entity)
    {
        if (!$this->doesObjectExists($entity)) {
            $this->getDirectoryService()->createParentDirectoryIfNotExist($entity);
            symlink($entity->getTarget(), $entity->getPath());
        }
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    public function removeObject($entity)
    {
        if ($this->doesObjectExists($entity)) {
            unlink($entity->getPath());
        }
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return bool
     */
    public function doesObjectExists($entity)
    {
        return is_link($entity->getPath());
    }
}
