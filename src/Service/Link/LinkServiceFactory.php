<?php

namespace PostInstall\Service\Link;

use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Base\Utils\FactoryInterface;

/**
 * PostInstall\Service\Link\LinkServiceFactory
 * @package mihac\PostInstall\Service\Link
 */
class LinkServiceFactory implements FactoryInterface
{
    use ServiceManagerAwareTrait;

    /**
     * Create Link service
     *
     * @return LinkService
     */
    public function create()
    {
        $service = new LinkService();
        $service->setDirectoryService($this->getServiceManager()->get('directory'));
        return $service;
    }
}
