<?php

namespace PostInstall\Service\Directory;

/**
 * PostInstall\Service\Directory\DirectoryServiceAwareTrait
 * @package mihac\PostInstall\Service\Directory
 */
trait DirectoryServiceAwareTrait
{
    /** @var DirectoryService */
    protected $directoryService;

    /**
     * @return DirectoryService
     */
    public function getDirectoryService()
    {
        if (!($this->directoryService instanceof DirectoryService)) {
            throw new \RuntimeException('Directory service is not set!');
        }
        return $this->directoryService;
    }

    /**
     * @param DirectoryService $directoryService
     */
    public function setDirectoryService($directoryService)
    {
        $this->directoryService = $directoryService;
    }
}
