<?php

namespace PostInstall\Service\Directory;

use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Entity\PathAwareInterface;
use PostInstall\Service\AbstractService;

/**
 * PostInstall\Service\Directory\DirectoryService
 * @package mihac\PostInstall\Service\Directory
 */
class DirectoryService extends AbstractService
{
    /**
     * @inheritdoc
     */
    public function createObject($entity)
    {
        if (!$this->doesObjectExists($entity)) {
            mkdir($entity->getPath(), 0777, true);
        }
    }
    /**
     * @inheritdoc
     */
    public function removeObject($entity)
    {
        if ($this->doesObjectExists($entity)) {
            $this->removeDirectoryRecursively($entity);
        }
    }

    /**
     * @inheritdoc
     */
    public function doesObjectExists($entity)
    {
        $path = $entity->getPath();
        return is_dir($path);
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    public function removeDirectoryRecursively($entity)
    {
        $dir = $entity->getPath();

        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    $tmpPath = $dir."/".$object;
                    if (is_dir($tmpPath) && !is_link($tmpPath)) {
                        $tmpEntity = new FormattedConfigEntity();
                        $tmpEntity->setPath($tmpPath);
                        $this->removeDirectoryRecursively($tmpEntity);
                    }
                    else {
                        unlink($tmpPath);
                    }
                }
            }
            rmdir($dir);
        }
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    public function createParentDirectoryIfNotExist($entity)
    {
        $dirName = dirname($entity->getPath());
        $entity = new FormattedConfigEntity();
        $entity->setPath($dirName);
        $this->createObject($entity);
    }
}
