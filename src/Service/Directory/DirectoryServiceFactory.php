<?php

namespace PostInstall\Service\Directory;

use PostInstall\Base\Utils\FactoryInterface;

/**
 * PostInstall\Service\Directory\DirectoryServiceFactory
 * @package mihac\PostInstall\Service\Directory
 */
class DirectoryServiceFactory implements FactoryInterface
{
    public function create()
    {
        $service = new DirectoryService();
        return $service;
    }
}
