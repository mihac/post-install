<?php

namespace PostInstall\Service\File;

use PostInstall\Entity\PathAwareInterface;
use PostInstall\Service\AbstractService;
use PostInstall\Service\Directory\DirectoryServiceAwareTrait;

/**
 * PostInstall\Service\File\FileService
 * @package mihac\PostInstall\Service\File
 */
class FileService extends AbstractService
{
    use DirectoryServiceAwareTrait;

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    public function createObject($entity)
    {
        if (!file_exists($entity->getPath())) {
            $this->getDirectoryService()->createParentDirectoryIfNotExist($entity);
            touch($entity->getPath());
        }
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return void
     */
    public function removeObject($entity)
    {
        if (file_exists($entity->getPath())) {
            unlink($entity->getPath());
        }
    }

    /**
     * @param PathAwareInterface $entity
     *
     * @return bool
     */
    public function doesObjectExists($entity)
    {
        return file_exists($entity->getPath());
    }
}
