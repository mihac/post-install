<?php

namespace PostInstall\Service\File;

use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Base\Utils\FactoryInterface;

/**
 * PostInstall\Service\File\FileServiceFactory
 * @package mihac\PostInstall\Service\File
 */
class FileServiceFactory implements FactoryInterface
{
    use ServiceManagerAwareTrait;

    /**
     * Create File service
     *
     * @return FileService
     */
    public function create()
    {
        $service = new FileService();
        $service->setDirectoryService($this->getServiceManager()->get('directory'));
        return $service;
    }
}
