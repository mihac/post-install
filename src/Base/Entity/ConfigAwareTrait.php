<?php

namespace PostInstall\Base\Entity;

/**
 * PostInstall\Base\Entity\ConfigAwareTrait
 * @package mihac\Base\Entity
 */
trait ConfigAwareTrait
{
    /** @var array $config */
    protected $config = array();

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     * @throws \DomainException
     */
    public function setConfig($config)
    {
        if (!is_array($config)) {
            throw new \DomainException('Configuration is not an array');
        }
        $this->config = $config;
    }
}
