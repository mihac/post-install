<?php

namespace PostInstall\Base\Utils;

/**
 * PostInstallTest\Base\Utils\FactoryInterface
 * @package mihac\Base\Utils
 */
interface FactoryInterface
{
    public function create();
}
