<?php

namespace PostInstall\Base\Service;

/**
 * PostInstall\Base\Service\ServiceManagerAwareTrait
 * @package mihac\PostInstall\Base\Service
 */
trait ServiceManagerAwareTrait
{
    /** @var ServiceManager */
    protected $serviceManager;

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        if (!$this->serviceManager) {
            $this->serviceManager = new ServiceManager();
        }
        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager($serviceManager)
    {
        if (!($serviceManager instanceof ServiceManager)) {
            throw new \DomainException('Object is not instance of ServiceManager');
        }
        $this->serviceManager = $serviceManager;
    }
}
