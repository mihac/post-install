<?php

namespace PostInstall\Base\Service;

use PostInstall\Base\Utils\FactoryInterface;

/**
 * PostInstall\Base\Service\ServiceManager
 * @package mihac\PostInstall\Base\Service
 */
class ServiceManager
{
    public function getConfig()
    {
        return include(__DIR__ . '/../../../config/config.php');
    }

    public function get($service)
    {
        $config = $this->getConfig();

        if(!isset($config['service_manager'])) {
            throw new \DomainException('Service manager is not configured, please check service_manager key in config file');
        }

        $invokables = array();
        if (isset($config['service_manager']['invokables'])) {
            $invokables = $config['service_manager']['invokables'];
        }

        $factories = array();
        if (isset($config['service_manager']['factories'])) {
            $factories = $config['service_manager']['factories'];
        }

        foreach($invokables as $invokable => $className) {
            if ($invokable == $service) {
                return new $className();
            }
        }

        foreach($factories as $factory=> $className) {
            if ($factory == $service) {
                /** @var FactoryInterface $factoryInstance */
                $factoryInstance =  new $className();
                if (!($factoryInstance instanceof FactoryInterface)) {
                    throw new \DomainException('Factory is not instance of FactoryInterface');
                }
                return $factoryInstance->create();
            }
        }

        throw new \DomainException("Service $service can't be created");
    }
}
