<?php

namespace PostInstall\Base\Filter;

/**
 * PostInstall\Base\Filter\FilterInterface
 * @package mihac\PostInstall\Base\Filter
 */
interface FilterInterface
{
    /**
     * Filter object
     *
     * @param mixed $object
     *
     * @return mixed
     */
    public function filter($object);
}
