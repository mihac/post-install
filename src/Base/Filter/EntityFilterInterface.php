<?php

namespace PostInstall\Base\Filter;

/**
 * PostInstall\Base\Filter\EntityFilterInterface
 * @package mihac\PostInstall\Base\Filter
 */
interface EntityFilterInterface
{
    public function mapArrayToObject($array, $object);
}
