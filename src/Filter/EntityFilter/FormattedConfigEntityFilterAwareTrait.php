<?php

namespace PostInstall\Filter\EntityFilter;

/**
 * PostInstall\Filter\EntityFilter\FormattedConfigEntityFilterAwareTrait
 * @package mihac\PostInstall\Filter\EntityFilter
 */
trait FormattedConfigEntityFilterAwareTrait
{
    /** @var FormattedConfigEntityFilter */
    protected $formattedConfigEntityFilter;

    /**
     * @return FormattedConfigEntityFilter
     */
    public function getFormattedConfigEntityFilter()
    {
        if (!$this->formattedConfigEntityFilter) {
            $this->formattedConfigEntityFilter = new FormattedConfigEntityFilter();
        }
        return $this->formattedConfigEntityFilter;
    }

    /**
     * @param FormattedConfigEntityFilter $formattedConfigEntityFilter
     */
    public function setFormattedConfigEntityFilter($formattedConfigEntityFilter)
    {
        $this->formattedConfigEntityFilter = $formattedConfigEntityFilter;
    }
}
