<?php

namespace PostInstall\Filter\EntityFilter;

use PostInstall\Base\Filter\EntityFilterInterface;
use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Entity\FormattedConfigEntity;

/**
 * PostInstall\Filter\EntityFilter\FormattedConfigEntityFilter
 * @package mihac\PostInstall\Filter\EntityFilter
 */
class FormattedConfigEntityFilter implements EntityFilterInterface
{
    use ServiceManagerAwareTrait;

    /**
     * @param array $array
     * @param FormattedConfigEntity $object
     *
     * @return FormattedConfigEntity
     */
    public function mapArrayToObject($array, $object)
    {
        $isValid = $this->checkIfConfigurationHasAllRequiredKeys($array);
        if (!$isValid) {
            throw new \DomainException("Configuration doesn't contain all required keys");
        }

        $object->setService($this->getServiceManager()->get($array['type']));
        $object->setPath($array['path']);
        $object->setUser($this->determineUser($array));
        $object->setGroup($this->determineGroup($array));
        $object->setMode($this->determineMode($array));
        $object->setReplace($this->determineReplacement($array));
        $object->setTarget($this->determineTarget($array));
        $object->setBackup($this->determineBackup($array));

        return $object;
    }

    public function getMandatoryConfigKeys($type)
    {
        $config = $this->getServiceManager()->getConfig();

        switch ($type) {
            case 'link':
                return $config['mandatory_fields_link'];
            default:
                return $config['mandatory_fields'];
        }
    }

    public function determineReplacement($config)
    {
        $replace = (isset($config['replace']) && $config['replace'] ? 1 : 0);

        return $replace;
    }

    public function determineUser($config)
    {
        $user = (isset($config['user']) ? $config['user'] : 0);

        return $user;
    }

    public function determineGroup($config)
    {
        $group = (isset($config['group']) ? $config['group'] : 0);

        return $group;
    }

    public function determineMode($config)
    {
        $mode = (isset($config['mode']) ? $config['mode'] : 0);

        return $mode;
    }

    public function determineTarget($config)
    {
        $target = (isset($config['target']) ? $config['target'] : 0);

        return $target;
    }

    public function determineBackup($config)
    {
        $backup = (isset($config['backup']) ? $config['backup'] : 0);

        return $backup;
    }

    public function checkIfConfigurationHasAllRequiredKeys($config)
    {
        $keys = array_keys($config);

        if (!isset($config['type'])) {
            throw new \DomainException('Type is not set');
        }

        $mandatoryConfigKeys = $this->getMandatoryConfigKeys($config['type']);
        $diff = array_diff($mandatoryConfigKeys, $keys);

        return empty($diff);
    }
}
