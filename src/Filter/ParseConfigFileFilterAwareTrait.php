<?php

namespace PostInstall\Filter;

/**
 * PostInstall\Filter\ParseConfigFileFilterAwareTrait
 * @package mihac\PostInstall\Filter
 */
trait ParseConfigFileFilterAwareTrait
{
    /** @var ParseConfigFileFilter */
    protected $parseConfigFileFilter;

    /**
     * @return ParseConfigFileFilter
     */
    public function getParseConfigFileFilter()
    {
        if (!$this->parseConfigFileFilter) {
            $this->parseConfigFileFilter = new ParseConfigFileFilter();
        }
        return $this->parseConfigFileFilter;
    }

    /**
     * @param ParseConfigFileFilter $parseConfigFileFilter
     */
    public function setParseConfigFileFilter($parseConfigFileFilter)
    {
        if (!($parseConfigFileFilter instanceof ParseConfigFileFilter)) {
            throw new \DomainException('Object is not instance of ParseConfigFileFilter');
        }
        $this->parseConfigFileFilter = $parseConfigFileFilter;
    }
}
