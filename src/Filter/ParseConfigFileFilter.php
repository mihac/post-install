<?php

namespace PostInstall\Filter;

use PostInstall\Base\Filter\FilterInterface;
use PostInstall\Base\Service\ServiceManagerAwareTrait;
use PostInstall\Entity\FormattedConfigEntity;
use PostInstall\Filter\EntityFilter\FormattedConfigEntityFilterAwareTrait;

/**
 * PostInstall\Filter\ParseConfigFileFilter
 * @package mihac\PostInstall\Filter
 */
class ParseConfigFileFilter implements FilterInterface
{
    use ServiceManagerAwareTrait;
    use FormattedConfigEntityFilterAwareTrait;

    /**
     * Filter $config
     *
     * @param array $configArray
     *
     * @return array
     */
    public function filter($configArray)
    {
        $filteredConfiguration = array();
        foreach ($configArray as $config) {
            /** @var FormattedConfigEntity $entity */
            $entity = $this->getFormattedConfigEntityFilter()->mapArrayToObject($config, new FormattedConfigEntity());
            $filteredConfiguration[] = $entity;
        }

        return $filteredConfiguration;
    }
}
