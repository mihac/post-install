<?php

$directoryMandatoryFields = array(
    'type',
    'path',
);

$mandatory_fields_link = array_merge(
    $directoryMandatoryFields,
    array(
        'linkTarget',
    )
);

return array(
    'service_manager'       => array(
        'invokables' => array(),
        'factories'  => array(
            'directory' => 'PostInstall\Service\Directory\DirectoryServiceFactory',
            'file'      => 'PostInstall\Service\File\FileServiceFactory',
            'link'      => 'PostInstall\Service\Link\LinkServiceFactory',
        ),
    ),
    'mandatory_fields'      => $directoryMandatoryFields,
    'mandatory_fields_link' => $mandatory_fields_link,
);
