# Post install script #

### What is this repository for? ###

* This script can create directory, files or symlinks for you

### How do I get set up? ###

* Configuration

    Create configuration as array
    <code>
    
        $config = array(
            array(
                'type'    => \PostInstall\Entity\ObjectType::DIRECTORY,
                'path'    => '/tmp/mix/test1',
                'user'    => 'mix',
                'group'   => 'mix',
                'mode'    => '0777',
                'replace' => true,
                'backup'  => true,
            ),
            array(
                'type' => \PostInstall\Entity\ObjectType::DIRECTORY,
                'path' => '/tmp/mix/test2',
            ),
        );
    </code>
    And pass it to the service
<code>

        $service = new PostInstall\Service\PostInstallService();
        $service->init($config);
</code>

* Directory creation
    * type _mandatory_ **string** \PostInstall\Entity\ObjectType::DIRECTORY
    * path _mandatory_ **string** directory path
    * user _optional_ **string** chown
    * group _optional_ **string** chgrp
    * mode _optional_ **string** chmod
    * replace _optional_ **boolean** replace old object with new one, default false
    * backup _optional_ **boolean** will rename old object to $oldName . '_' . date('Y-m-d_h:i:s'), default false
* File creation
    * type _mandatory_ **string** \PostInstall\Entity\ObjectType::FILE,
    * path _mandatory_ **string** file path
    * user _optional_ **string** chown
    * group _optional_ **string** chgrp
    * mode _optional_ **string** chmod
    * replace _optional_ **boolean** replace old object with new one, default false
    * backup _optional_ **boolean** will rename old object to $oldName . '_' . date('Y-m-d_h:i:s'), default false
* Symlink creation
    * type _mandatory_ **string** \PostInstall\Entity\ObjectType::LINK,
    * path _mandatory_ **string** symlink path
    * target _mandatory_ **string** target directory/file path
    * user _optional_ **string** chown
    * group _optional_ **string** chgrp
    * mode _optional_ **string** chmod
    * replace _optional_ **boolean** replace old object with new one, default false
    * backup _optional_ **boolean** will rename old object to $oldName . '_' . date('Y-m-d_h:i:s'), default false
